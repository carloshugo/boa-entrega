﻿using Confluent.Kafka;
using System;
using System.Threading.Tasks;

namespace BoaEntrega.Commons.Message
{
    public abstract class AbstractKafkaSender : IMessageSender
    {
        private readonly string _bootstrapServers;
        private readonly string _topic;

        protected AbstractKafkaSender(string bootstrapServers, string topic)
        {
            _bootstrapServers = bootstrapServers ?? throw new ArgumentNullException(nameof(bootstrapServers));
            _topic = topic ?? throw new ArgumentNullException(nameof(topic));
        }

        public async Task Enviar(string mensagem)
        {
            var config = new ProducerConfig { BootstrapServers = _bootstrapServers };
            using var producer = new ProducerBuilder<Null, string>(config).Build();
            await producer.ProduceAsync(_topic, new Message<Null, string> { Value = mensagem });
        }
    }
}
