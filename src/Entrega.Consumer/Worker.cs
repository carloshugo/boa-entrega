using Entrega.Bloc.Entities;
using Entrega.Consumer.Message;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Entrega.Consumer
{
    public class Worker : BackgroundService
    {
        private readonly IMessageHandler<string> _handlerEntregaFinalizada;
        private readonly IMessageHandler<Transporte> _handlerLocalizacaoAtualizada;
        public Worker(IMessageHandler<string> handlerEntregaFinalizaca, IMessageHandler<Transporte>  handlerLocalizacaoAtualizada)
        {
            _handlerEntregaFinalizada = handlerEntregaFinalizaca ?? throw new ArgumentNullException(nameof(handlerEntregaFinalizaca));
            _handlerLocalizacaoAtualizada = handlerLocalizacaoAtualizada ?? throw new ArgumentNullException(nameof(handlerLocalizacaoAtualizada));
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _handlerEntregaFinalizada.MessageHandler();
            _handlerLocalizacaoAtualizada.MessageHandler();
            return Task.CompletedTask;
        }
    }
}
