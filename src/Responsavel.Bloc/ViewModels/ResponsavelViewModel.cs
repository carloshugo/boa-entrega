﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Responsavel.Bloc.ViewModels
{
    public class ResponsavelViewModel
    {
        public string Id { get; set; }
        public string Nome { get; set; }
    }
}
