﻿using System.Threading.Tasks;

namespace Responsavel.Bloc.Repositories
{
    public interface IResponsavelRepository
    {
        Task Atualizar(Entities.Responsavel responsavel);
        Task<Entities.Responsavel> Obter(string id);
        Task<Entities.Responsavel> ObterPorTransporteId(string transporteId);
    }
}
