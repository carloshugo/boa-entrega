﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;

namespace Responsavel.Bloc.Data
{
    public abstract class ResponsavelAbstractContext : IResponsavelContext
    {
        private readonly MongoClient _client;
        private readonly IMongoDatabase _database;
        private readonly string _databaseName;
        private readonly string _collectionName;

        public ResponsavelAbstractContext(string databaseName, string connectionString, string collectionName)
        {
            _collectionName = collectionName ?? throw new ArgumentNullException(nameof(collectionName));
            _databaseName = databaseName ?? throw new ArgumentNullException(nameof(databaseName));
            _client = new MongoClient(connectionString);
            _database = _client.GetDatabase(databaseName);
            if (CollectionExists())
            {
                Responsaveis = _database.GetCollection<Entities.Responsavel>(collectionName);
            }
            else
            {
                _database.CreateCollection(collectionName);
                Responsaveis = _database.GetCollection<Entities.Responsavel>(collectionName);
            }
            ResponsavelContextSeed.SeedData(Responsaveis);
        }

        public IMongoCollection<Entities.Responsavel> Responsaveis { get; }

        private IMongoDatabase GetDatabase() => _client.GetDatabase(_databaseName);
        private bool CollectionExists()
        {
            var filter = new BsonDocument("name", _collectionName);
            var collections = GetDatabase().ListCollectionsAsync(new ListCollectionsOptions { Filter = filter });
            return collections.Result.Any();
        }
        
    }
}
