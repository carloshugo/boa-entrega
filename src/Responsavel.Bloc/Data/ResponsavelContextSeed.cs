﻿using MongoDB.Driver;
using Responsavel.Bloc.Entities.Enums;
using System;
using System.Collections.Generic;

namespace Responsavel.Bloc.Data
{
    public class ResponsavelContextSeed
    {
        public static void SeedData(IMongoCollection<Entities.Responsavel> productCollection)
        {
            bool existProduct = productCollection.Find(p => true).Any();
            if (!existProduct)
            {
                productCollection.InsertManyAsync(ObterResponsaveis());
            }
        }

        private static IEnumerable<Entities.Responsavel> ObterResponsaveis()
        {
            DateTime agora = DateTime.Now;
            int numero = 5;
            return new List<Entities.Responsavel>()
            {
                new Entities.Responsavel("Kleber")
                {
                    Id = "622639ee50baa69274b1d2fa",
                    Entregas = new List<Entities.Entrega>
                    {
                        new Entities.Entrega
                        {
                            Id = "6232a63d621462f9da9fd6de",
                            Status = StatusEntrega.Andamento,
                            Carga = "Celulares Moto G20",
                            Destinatario = "Casa Grande. CNPJ 64.767.749/0001-41",
                            Cliente = "Mercadão dos celulares. CNPJ 57.058.338/0001-11",
                            Rota = "Ponto A, B, C, D",
                            PrazoEmDiasUteis = numero,
                            Transporte =
                            new Entities.Transporte {
                                Id = "6233eaf9dc2b6fa11c5576a3",
                                Nome = "Caminhao I",
                                Latitude = new decimal(-1.4),
                                Longitude = new decimal(-27.13)
                            },
                            Inicio = agora.AddDays(-numero),
                            Fim = agora,
                        }
                    }
                },
                new Entities.Responsavel("Hugo")
                {
                    Id = "62263a03da0fa5a38136e2ca"
                },
                new Entities.Responsavel("João")
                {
                    Id = "62263a0f4b8842bab4ec9e51"
                },
                new Entities.Responsavel("Maria")
                {
                    Id = "6226d1239e6f7ed2facdc9b3",
                }
            };
        }
    }
}
