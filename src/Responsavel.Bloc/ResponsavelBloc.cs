﻿using BoaEntrega.Commons;
using Responsavel.Bloc.Converter;
using Responsavel.Bloc.Entities;
using Responsavel.Bloc.Repositories;
using Responsavel.Bloc.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Responsavel.Bloc
{
    public class ResponsavelBloc : IResponsavelBloc
    {
        private readonly IResponsavelRepository _repository;
        private readonly IMessageSender _sender;

        public ResponsavelBloc(IResponsavelRepository repository, IMessageSender sender = null)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _sender = sender;
        }

        public async Task AdicionarEntrega(EntregaViewModel entregaViewModel)
        {
            Entities.Responsavel responsavel = await _repository.Obter(entregaViewModel.Responsavel.Id);
            if (responsavel.Entregas == null)
            {
                responsavel.Entregas = new List<Entrega>();
            }
            Entrega entrega = EntregaViewModelConverter.ToEntrega(entregaViewModel);
            responsavel.Entregas.Add(entrega);
            await _repository.Atualizar(responsavel);
        }

        public async Task AtualizacaoLocalizacaoTransporte(Transporte transporte)
        {
            var responsavel = await _repository.ObterPorTransporteId(transporte.Id);
            if (responsavel != null)
            {
                var entrega = responsavel.Entregas.Find(e => e.Transporte.Id.Equals(transporte.Id));
                responsavel.Entregas.Remove(entrega);
                entrega.Transporte = transporte;
                responsavel.Entregas.Add(entrega);
                await _repository.Atualizar(responsavel);

            }
        }

        public async Task FinalizarEntrega(string idResponsavel, string idEntrega)
        {
            Entities.Responsavel responsavel = await _repository.Obter(idResponsavel);
            Entrega entrega = responsavel.Entregas.Find(e => e.Id.Equals(idEntrega));
            responsavel.Entregas.Remove(entrega);
            
            await _repository.Atualizar(responsavel);

            await _sender.Enviar(entrega.Id);
        }

        public async Task<Entities.Responsavel> Obter(string id)
        {
            return await _repository.Obter(id);
        }

    }
}
