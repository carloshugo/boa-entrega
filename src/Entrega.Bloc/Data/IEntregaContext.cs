﻿using MongoDB.Driver;

namespace Entrega.Bloc.Data
{
    public interface IEntregaContext
    {
        IMongoCollection<Entities.Entrega> Entregas { get; }
    }
}
