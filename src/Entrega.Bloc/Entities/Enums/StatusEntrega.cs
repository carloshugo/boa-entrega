﻿namespace Entrega.Bloc.Entities.Enums
{
    public enum StatusEntrega
    {
        Criada,
        Andamento,
        Impedida,
        Finalizada
    }
}
