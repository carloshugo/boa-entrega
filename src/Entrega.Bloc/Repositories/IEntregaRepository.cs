﻿using System.Threading.Tasks;

namespace Entrega.Bloc.Repositories
{
    public interface IEntregaRepository
    {
        Task<Entities.Entrega> ObterPorTransporteId(string transporteId);
        Task Criar(Entities.Entrega entrega);
        Task Atualizar(Entities.Entrega entrega);
        Task<Entities.Entrega> Obter(string id);
    }
}
