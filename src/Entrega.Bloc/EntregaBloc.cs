﻿using BoaEntrega.Commons;
using BoaEntrega.Commons.Converters;
using Entrega.Bloc.Entities;
using Entrega.Bloc.Entities.Enums;
using Entrega.Bloc.Repositories;
using System;
using System.Threading.Tasks;

namespace Entrega.Bloc
{
    public class EntregaBloc : IEntregaBloc
    {
        private readonly IEntregaRepository _repository;
        private readonly IMessageSender _sender;
        private readonly IJsonConverter<Entities.Entrega> _json;

        public EntregaBloc(IEntregaRepository repository, IJsonConverter<Entities.Entrega> json, IMessageSender sender = null)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _sender = sender;
            _json = json ?? throw new ArgumentNullException(nameof(json));
        }

        public async Task Criar(Entities.Entrega entrega)
        {
            entrega.Status = StatusEntrega.Criada;
            await _repository.Criar(entrega);
            string entregaJson = _json.Serialize(entrega);
            await _sender.Enviar(entregaJson);
        }

        public async Task FinalizarEntrega(string id)
        {
            Entities.Entrega entrega = await _repository.Obter(id);
            entrega.Status = StatusEntrega.Finalizada;
            entrega.DataEntrega = DateTime.Now;
            await _repository.Atualizar(entrega);
        }

        public async Task<Entities.Entrega> Obter(string id)
        {
            return await _repository.Obter(id);
        }

        public async Task AtualizacaoLocalizacaoTransporte(Transporte transporte)
        {
            var entrega = await _repository.ObterPorTransporteId(transporte.Id);
            if (entrega != null)
            {
                entrega.Transporte = transporte;
                await _repository.Atualizar(entrega);
            }
        }

    }
}