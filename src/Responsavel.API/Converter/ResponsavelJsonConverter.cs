﻿using BoaEntrega.Commons.Converters;
using System.Text.Json;

namespace Responsavel.API.Converter
{
    public class ResponsavelJsonConverter : IJsonConverter<Bloc.Entities.Responsavel>
    {
        public Bloc.Entities.Responsavel Deserialize(string json)
        {
            return JsonSerializer.Deserialize<Bloc.Entities.Responsavel>(json);
        }

        public string Serialize(Bloc.Entities.Responsavel obj)
        {
            return JsonSerializer.Serialize(obj);
        }
    }
}
