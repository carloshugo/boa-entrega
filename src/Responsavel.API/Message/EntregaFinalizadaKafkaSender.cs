﻿using BoaEntrega.Commons.Message;
using Microsoft.Extensions.Configuration;

namespace Responsavel.API.Message
{
    public class EntregaFinalizadaKafkaSender : AbstractKafkaSender
    {
        public EntregaFinalizadaKafkaSender(IConfiguration configuration) : 
            base(configuration.GetValue<string>("KafkaSettings:BootstrapServers"),
                Topicos.EntregasFinalizadas.ToString())
        {
        }
    }
}
