﻿using BoaEntrega.Commons.Message;
using Microsoft.Extensions.Configuration;

namespace Entrega.API.Message
{
    public class EntregaCriadaKafkaSender : AbstractKafkaSender
    {
        public EntregaCriadaKafkaSender(IConfiguration configuration) : 
            base(configuration.GetValue<string>("KafkaSettings:BootstrapServers"),
                Topicos.EntregasCriadas.ToString())
        {
        }
    }
}
