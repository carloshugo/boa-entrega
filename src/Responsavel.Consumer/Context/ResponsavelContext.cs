﻿using Responsavel.Bloc.Data;
using Microsoft.Extensions.Configuration;

namespace Responsavel.Consumer.Context
{
    public class ResponsavelContext : ResponsavelAbstractContext
    {
        public ResponsavelContext(IConfiguration configuration) : 
            base(
                configuration.GetValue<string>("DatabaseSettings:DatabaseName"), 
                configuration.GetValue<string>("DatabaseSettings:ConnectionString"),
                configuration.GetValue<string>("DatabaseSettings:CollectionName"))
        {
        }
    }
}
