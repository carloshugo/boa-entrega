using Microsoft.Extensions.Hosting;
using Responsavel.Bloc.Entities;
using Responsavel.Bloc.ViewModels;
using Responsavel.Consumer.Message;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Responsavel.Consumer
{
    public class Worker : BackgroundService
    {
        private readonly IMessageHandler<EntregaViewModel> _handlerEntregaCriada;
        private readonly IMessageHandler<Transporte> _handlerLocalizacaoAtualizada;
        public Worker(IMessageHandler<EntregaViewModel> handlerEntregaCriada, IMessageHandler<Transporte> handlerLocalizacaoAtualizada)
        {
            _handlerEntregaCriada = handlerEntregaCriada ?? throw new ArgumentNullException(nameof(handlerEntregaCriada));
            _handlerLocalizacaoAtualizada = handlerLocalizacaoAtualizada ?? throw new ArgumentNullException(nameof(handlerLocalizacaoAtualizada));
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _handlerLocalizacaoAtualizada.MessageHandler();
            _handlerEntregaCriada.MessageHandler();
            return Task.CompletedTask;
        }
    }
}
