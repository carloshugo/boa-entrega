﻿using BoaEntrega.Commons.Message;
using Microsoft.Extensions.Configuration;

namespace Responsavel.API.Message
{
    public class LocalizacaoAtualizadaKafkaSender : AbstractKafkaSender
    {
        public LocalizacaoAtualizadaKafkaSender(IConfiguration configuration) : 
            base(configuration.GetValue<string>("KafkaSettings:BootstrapServers"),
                Topicos.LocalizacaoTransporte.ToString())
        {
        }
    }
}
