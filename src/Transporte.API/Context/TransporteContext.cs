﻿using Microsoft.Extensions.Configuration;
using Transporte.Bloc.Data;

namespace Transporte.API.Context
{
    public class TransporteContext : TransporteAbstractContext
    {
        public TransporteContext(IConfiguration configuration) :
            base(
                configuration.GetValue<string>("DatabaseSettings:DatabaseName"),
                configuration.GetValue<string>("DatabaseSettings:ConnectionString"),
                configuration.GetValue<string>("DatabaseSettings:CollectionName"))
        {
        }
    }
}
